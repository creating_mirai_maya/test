//---------------------------
/*
flexboxで作った横並びボタンのテキストを上下センターで揃える
※ulにflexboxを必ず設定してください

記述例：
<ul>
	<li><a href="#">テキストテキストテキスト</a></li>
	<li><a href="#">テキスト</a></li>
	<li><a href="#">テキスト</a></li>
</ul>

<script type="text/javascript">flexCenter("ul li");</script>

表示例：
------　　　------　　　------
テキスト
テキスト　　 テキスト　　 テキスト　
テキスト
------　　　------　　　------
*/
//---------------------------
function flexCenter(pass){
	var arrayH = [];
	$(pass).each(function(i, elem) {
		arrayH.push($(elem).height());
		$(elem).children().css({
			"display" : "table-cell",
			"vertical-align" : "middle"
		});
	});
	maxH = Math.max.apply(null,arrayH);
	console.log("max："+maxH);	
	$(pass).each(function(i, elem) {
		$(elem).children().height(maxH);
	});
}
	

	
//---------------------------
//自分の高さを取得して、自分自身に高さを設定する
//例：<script type="text/javascript">myHeighGet(".events ul li");</script>
//---------------------------
function myHeighGet(pass){
	$(pass).each(function(i, elem) {
		thisHeight = $(elem).height();
		$(elem).css("height",thisHeight);
		console.log(i + ': ' + thisHeight);
	});
}
	



//---------------------------
//スクロールに合わせてアニメーション用のclassを付けたり外したり
/*
<section class="mv mv_fade mv_top">・・・・</section>
*/
//---------------------------

$(function(){
	//windowの高さ
	var windowHeight = $(window).height();

	//画面下から表示されてどれくらいでアニメーションスタートするか
	//適当に数字に変えてもいいです。
	var delay = windowHeight/6; 
	//var delay = 500; 

	//ロード時の初期値
	$(window).load(function() {
		$(".mv").each(function(){
			var mvPos = $(this).offset().top;
			if(mvPos<windowHeight-delay){
				//すでにアニメーション領域に表示されていたらアニメーションスタート
				$(this).stop().addClass('move');
			}
		});
	});

	/*スクロールしたら*/
	$(window).scroll(function (){
		$(".mv").each(function(){
			mvPos = $(this).offset().top;    
			scroll = $(window).scrollTop();

			thisPos = mvPos - windowHeight + delay;

			if (scroll > thisPos){
				$(this).stop().addClass('move');
			} else {
				//スクロールアニメーションを繰り返したくなければ下記をコメントアウト
				$(this).stop().removeClass('move');
			}

		});
	});
});


//---------------------------
//マウスオーバーでアニメーション用のclassを付けたり外したり
/*

１）マウスオーバーした要素の子の要素を動かす場合
　　<a href="#" class="mvhov"><img src="#" class="mvhov-tg mv_right">・・・・</a>

２）マウスオーバーした要素自身を動かす場合
　　<a href="#" class="mvhov mvhov-tg mv_top">・・・・</a>

*/
//---------------------------
$(function() {
	$('.mvhov').hover(
		function () {
			if ($(this).hasClass('mvhov-tg')) {
				console.log("do");
				$(this).addClass('move');
			}else{
				$(".mvhov-tg",this).addClass('move');
			}
		},
		function () {
			if ($(this).hasClass('mvhov-tg')) {
				console.log("do");
				$(this).removeClass('move');
			}else{
				$(".mvhov-tg",this).removeClass('move');
			}
		}
	);
});


$(function() {
	
//---------------------------
//
//遅延読み込み　lazyloadにローディング画像を追加
/*
・直接画像に設定するとき
<img class="lazyload" src="hoge/hoge.png" >
・背景画像に設定するとき
<div class="lazyload"> hogehoge </div>
*/
//---------------------------
$(".lazyload").append('<div class="lazyloader"><img src="/common/js/lazysizes/assets/imgs/loader.gif" width="30" height="30"></div>');


	
//---------------------------
/*複数業ellipsis対応*/
//---------------------------
$('.multiEllipsis').each(function() {
	var $target = $(this);

	// オリジナルの文章を取得する
	var html = $target.html();

	// 対象の要素を、高さにautoを指定し非表示で複製する
	var $clone = $target.clone();
	$clone
		.css({
			display: 'none',
			position : 'absolute',
			overflow : 'visible'
		})
		.width($target.width())
		.height('auto');

	// DOMを一旦追加
	$target.after($clone);

	// 指定した高さになるまで、1文字ずつ消去していく
	while((html.length > 0) && ($clone.height() > $target.height())) {
		html = html.substr(0, html.length - 1);
		$clone.html(html + '...');
	}

	// 文章を入れ替えて、複製した要素を削除する
	$target.html($clone.html());
	$clone.remove();
});
	

/*----------------------*/
/*文字数をカットして...をつける*/
/*
例）　<dt class="cuttxt" data="11">GERMANHOUSE「未来」</dt>
*/
/*----------------------*/	
$('.cuttxt').each(function(){
	var cutNum = $(this).attr("data");
	var textLength = $(this).text().length;
	var textTrim = $(this).text().substr(0,(cutNum))
	if(cutNum < textLength) {
		$(this).html(textTrim + '...');
	}
});


/*----------------------*/
/*初期変数設定*/
/*----------------------*/
//スクロールのスピード
var scrollspeed = 1000;

//スクロール量の微調整
//調整必要なしの場合は0
var scroll_cntrol = 0;

//fixnaviを表示するスクロール量
//var fixnaviscroll_position = 0;

/*----------------------*/
/*ページ内スクロール*/
/*<a class="pgscroll" href="#sitemap">サイトマップ</a>*/
/*----------------------*/
$(".pgscroll").click(function(event){
	event.preventDefault();
	        
	var url = this.href;
	var parts = url.split("#");
	var target = parts[1];
	var target_offset = $("#"+target).offset();
	var target_top = target_offset.top+scroll_cntrol;
	
	$('html, body').animate({scrollTop:target_top}, scrollspeed);
});
	

//---------------------------
//<div class="wrapper">を追加
//---------------------------
$(".js_wrap").wrapInner("<div class='wrap'></div>");

});


/*----------------------*/
/*ウィンドウサイズ取得*/
/*コンテンツの高さを設定*/
/*----------------------*/
/*
var winW;
var winH;
$(document).ready(function(){
	winW = $(window).width();
	winH = $(window).height();
});
$(window).resize(function(){
	winW = $(window).width();
	winH = $(window).height();
});
*/