$(function() {

/*----------------------*/
/*スマホメニュー*/
/*----------------------*/
$('.smp_head_menu').click(function()
	{
		$(this).toggleClass("active");
		if ($('nav#glnavi').css('display') == 'none'){
			$("nav#glnavi").slideDown();
		} else {
			$("nav#glnavi").slideUp();
		}
	}
);


/*----------------------*/
/*スマホローカルメニュー*/
/*----------------------*/
var fixposition = $("header").height();
var lcnv_height = $("#localnav").height()+2;
$(window).scroll(function () {
	if ($(this).scrollTop() > fixposition) {
		$("#localnav").css({
			"position":"fixed",
			"top":0,
			"z-index":999999
		});
		$("header").css({
			"margin-bottom":lcnv_height
		});
		
	} else {
		$("#localnav").css({
			"position":"relative",
			"top":0
		});
		$("header").css({
			"margin-bottom":0
		});
	}
});

$('#bt_lcnvmenu').click(
	function() {
		if ($('.subnav').css('display') == 'none'){
			$(".subnav").slideDown();
			$(this).removeClass("close");
			$(this).addClass('open');
		} else {
			$(".subnav").slideUp();
			$(this).removeClass("open");
			$(this).addClass('close');
		}
	}
);



});
